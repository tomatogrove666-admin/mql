#include <_asktf_sample.mqh>
#property   strict
#property   indicator_chart_window 
#property   indicator_buffers 1 
#property   indicator_color1 clrNONE 
#property   indicator_width1 1

string TFS=" ";
//-------------
extern bool prn = 0;
double opentf[];

int OnInit(void) 
  { 
   string short_name="_asktf";
   oindi1(short_name,TFS,opentf);
   return_isu;
  } 
void OnDeinit_df1 
int Oncalculate_df1
  { 
   if (prev_calculated==0)ArrayInitialize(opentf,EMPTY_VALUE);
   opentf[0]=close[0];
   if (prn) Print_df1("_asktf OnC: ", time[0], 5, open[0], opentf[0]);
   return_rt; 
  }
